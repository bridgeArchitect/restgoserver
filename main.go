package main

import (
	"Function4"
	"database/sql"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

/* structure to save instance of "JSON's" value for function */
type Number struct {
	Value int `json:"Value"`
}

/* structure to save instance of "JSON's" writer */
type Writer struct {
	ID          string `json:"ID"`
	FirstName   string `json:"FirstName"`
	MiddleName  string `json:"MiddleName"`
	LastName    string `json:"LastName"`
	MainWork    string `json:"MainWork"`
	IDGenre     string `json:"IDGenre"`
}

/* constant rows to open database */
const (
	server = "mysql"
	data = "go:Moskow2012@/WriterDatabase"
)

/* path to error logfile */
const (
	errfile    = "/home/bridgearchitect/SP/Lab7/RestGoServer/error.log"
)

/* function to handle error */
func handleError(err error) bool {

	/* handle error */
	if err != nil {

		var (
			fileErr *os.File
		)

		/* write into error logfile */
		fileErr, _ = os.OpenFile(errfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		_, _ = fileErr.WriteString(time.Now().String() + ":" + err.Error() + "\n")

		/* error exists */
		return true

	}

	/* error does not exist */
	return false

}

/* function to open database using ORM */
func openDatabase() *sql.DB {

	/* declaration of variables */
	var (
		db         *sql.DB
		err 	   error
	)

	/* creation of connection */
	db, err = sql.Open(server, data)
	if handleError(err) {
		return nil
	}

	/* return database */
	return db

}

/* function to send value of function */
func getValueF4(respWriter http.ResponseWriter, req *http.Request) {

	/* declaration of variables */
	var (
		params   map[string]string
		x, y     int
		arrayRow []string
		err      error
		number   Number
	)

	/* receive numbers for function */
	params = mux.Vars(req)
	arrayRow = strings.Split(params["numbers"],"&")

	/* convert string to number */
	x, err = strconv.Atoi(arrayRow[0])
	if handleError(err) {
		return
	}
	y, err = strconv.Atoi(arrayRow[1])
	if handleError(err) {
		return
	}

	/* get value of function */
	number.Value = Function4.F4(x, y)

	/* send value of function */
	respWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(respWriter).Encode(number)
	if handleError(err) {
		return
	}

}

/* function to send table of writers */
func getWriters(respWriter http.ResponseWriter, req *http.Request) {

	/* declaration of variables */
	var (
		writers     []Writer
		currWriter  Writer
		db  		*sql.DB
		rows        *sql.Rows
		err         error
	)

	/* open mysql-database and make query */
	db = openDatabase()
	rows, err = db.Query("select * from writers")
	if handleError(err) {
		return
	}

	/* create array of writers */
	writers = make([]Writer, 0, 1000)
	for rows.Next() {
		err = rows.Scan(&currWriter.ID, &currWriter.FirstName, &currWriter.MiddleName, &currWriter.LastName, &currWriter.MainWork, &currWriter.IDGenre)
		if handleError(err) {
			return
		}
		writers = append(writers, currWriter)
	}

	/* send table of writers */
	respWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(respWriter).Encode(writers)
	if handleError(err) {
		return
	}

	/* close database */
	err = db.Close()
	if handleError(err) {
		return
	}

}

func getWriter(respWriter http.ResponseWriter, req *http.Request) {

	/* declaration of variables */
	var (
		resWriter   Writer
		db  		*sql.DB
		rows        *sql.Rows
		err         error
		params      map[string]string
		id          string
	)

	/* receive id of writer */
	params = mux.Vars(req)
	id = params["id"]

	/* convert string to number for checking */
	_, err = strconv.Atoi(id)
	if handleError(err) {
		return
	}

	/* open database and make query */
	db = openDatabase()
	rows, err = db.Query("select * from writers where id = ?", id)
	if handleError(err) {
		return
	}

	/* send record about one writer */
	respWriter.Header().Set("Content-Type", "application/json")
	if rows.Next() {

		err = rows.Scan(&resWriter.ID, &resWriter.FirstName, &resWriter.MiddleName, &resWriter.LastName, &resWriter.MainWork, &resWriter.IDGenre)
		if handleError(err) {
			return
		}

		err = json.NewEncoder(respWriter).Encode(resWriter)
		if handleError(err) {
			return
		}

	}

	/* close database */
	err = db.Close()
	if handleError(err) {
		return
	}

}

func main() {

	/* declaration of router */
	var (
		router *mux.Router
	)

	/* make handlers for function */
	router = mux.NewRouter()
	router.HandleFunc("/F4/{numbers}", getValueF4).Methods("GET")
	router.HandleFunc("/Writer", getWriters).Methods("GET")
	router.HandleFunc("/Writer/{id}", getWriter).Methods("GET")

	/* launch REST-service */
	log.Fatal(http.ListenAndServe(":8000", router))

}
